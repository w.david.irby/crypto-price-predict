%matplotlib inline
import math
from matplotlib.pyplot import close
from sklearn.datasets import load_wine
import torch
from torch import nn
from torch.nn import functional as F
from d2l import torch as d2l
import numpy as np
import pandas as pd

# reads data from csv
data = pd.read_csv (r'C:\Users\derv\Desktop\crypto-price-predict\Binance_XMRBTC_1h.csv')

clean_data = pd.DataFrame(data, columns= ['date', 'open', 'high', 'low', 'close', 'Volume BTC', 'tradecount'])

features = pd.DataFrame(data, columns = ['open', 'high', 'low'])

# print(clean_data)
#                   date      open      high       low     close  Volume BTC  tradecount
#    2022-05-23 00:00:00  0.006179  0.006225  0.006165  0.006212    8.556140       989.0
#    2022-05-22 23:00:00  0.006164  0.006191  0.006127  0.006179    7.979363       961.0
#    2022-05-22 22:00:00  0.006079  0.006227  0.006076  0.006164   11.798357      2211.0
#    2022-05-22 21:00:00  0.006023  0.006104  0.006013  0.006078   16.997450      1048.0

# First layer of neural net just predicts based on open, high, and/or low 
# 
# second, etc layer can take into account rest of training set, e.g. volume, tradecount
# Each date is an example/data point/sample/data instance
# The label/target will be predicting close
# The features/covariates will be open, high, low
# n will be 100 samples to start
# Bias/offset/intercept
# 
# 
# Our linearity assumption will be 
# close = (weight_open)(open) + 
#     (weight_high)(high) +(weight_low)(low) + bias
# 
# also the dot product of the weights vector 
#  and variables vector then plus bias
# 
# A type of affine transformation of input features
#  characterized by linear transformation of via weighted sums
#  combined with a translation via bias
# 
# Design matrix has examples for rows and features as columns
# 
# weights vector and bias are the model parameters
# 
# Fitness measure:
# Squared error loss function
#    error = (1/2) * (prediction_value - real_value)^2

def squared_loss(y_hat, y): #@save
"""Squared loss."""
return (y_hat - y.reshape(y_hat.shape)) ** 2 / 2

# 
# Gradient descent iteratively reducing error by updating parameters
# 
# 















